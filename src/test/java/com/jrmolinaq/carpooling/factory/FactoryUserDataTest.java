package com.jrmolinaq.carpooling.factory;

import com.jrmolinaq.carpooling.model.User;

public class FactoryUserDataTest {

    public static User getUserWithoutId() {
        return User.builder()
                .firstName("Pedro")
                .lastName("Perez")
                .phoneNumber(3573958L)
                .address("Cra 10 # 20 - 30")
                .email("pedro.perez@gmail.com")
                .password("Pedro@1990")
                .build();
    }

    public static User getUserWithId() {
        User user = getUserWithoutId();
        user.setUserId(1);
        return user;
    }
}
