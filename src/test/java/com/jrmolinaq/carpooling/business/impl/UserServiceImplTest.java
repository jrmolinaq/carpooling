package com.jrmolinaq.carpooling.business.impl;

import com.jrmolinaq.carpooling.factory.FactoryUserDataTest;
import com.jrmolinaq.carpooling.model.User;
import com.jrmolinaq.carpooling.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository userRepository;

    @Test
    void findUserById() {

    }

    @Test
    void mustSaveAUser() throws Exception {
        User userWithoutId = FactoryUserDataTest.getUserWithoutId();

        User userWithId = FactoryUserDataTest.getUserWithId();

        when(userRepository.save(userWithoutId)).thenReturn(userWithId);

        User response = userService.saveUser(userWithoutId);

        assertEquals(userWithId, response);
    }
}