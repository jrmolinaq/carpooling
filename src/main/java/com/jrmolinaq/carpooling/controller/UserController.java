package com.jrmolinaq.carpooling.controller;

import com.jrmolinaq.carpooling.business.UserService;
import com.jrmolinaq.carpooling.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{userId}")
    public ResponseEntity<User> findUserById(@PathVariable("userId") String userId) throws Exception {
        User user = userService.findUserById(userId);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<User> saveUser(@RequestBody User user) throws Exception {
        return new ResponseEntity<>(userService.saveUser(user), HttpStatus.CREATED);
    }
}
