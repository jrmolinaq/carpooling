package com.jrmolinaq.carpooling.util.constants;

public class Constants {

    public static final String BAD_REQUEST_USER_ID_MESSAGE = "El user ID no debe ser nulo y debe ser numérico.";

    public static final String USER_ALREADY_EXISTS_MESSAGE = "Ya existe un usuario en el sistema con el mismo user ID.";

    public static final String USER_INVALID_FIRST_NAME_MESSAGE =
            "El nombre del usuario es requerido y solo puede contener letras.";

    public static final String USER_INVALID_LAST_NAME_MESSAGE =
            "El apellido del usuario es requerido y solo puede contener letras.";

    public static final String USER_INVALID_PHONE_NUMBER_MESSAGE =
            "El número telefónico es requerido y solo puede contener números.";

    public static final String USER_INVALID_ADDRESS_MESSAGE =
            "El dirección es requerida y solo puede contener letras, números y los simbolos _-#.,:()";

    public static final String USER_INVALID_EMAIL_MESSAGE =
            "El correo electrónico es requerido y debe tener una composición correcta.";

    public static final String USER_INVALID_PASSWORD_MESSAGE =
            "La contraseña es requerida y debe contener mayúsculas, minusculas, números y caracteres especiales.";

    public static final String USER_INVALID_PASSWORD_SIZE_MESSAGE =
            "La contraseña debe tener entre 8 y 15 caracteres.";

    public static final String NUMERIC_PATTERN = "^[\\d]+$";

    public static final String LETTERS_PATTERN = "^[a-zA-Z]+$";

    public static final String ADDRESS_PATTERN = "^[\\w\\s\\-\\#\\.\\,\\:\\(\\)]+$";

    public static final String EMAIL_PATTERN = "^[\\w-\\.]+@([\\w-]+){1}(\\.[a-zA-Z]{2,4}){1,2}$";

    public static final String PASSWORD_PATTERN = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])((?=.*\\W)|(?=.*_))^[^ ]+$";

    public static final String PASSWORD_SIZE_PATTERN = "^.{8,15}$";
}
