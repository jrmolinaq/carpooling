package com.jrmolinaq.carpooling.util;

import com.jrmolinaq.carpooling.exceptions.BadRequestException;
import com.jrmolinaq.carpooling.model.User;
import com.jrmolinaq.carpooling.util.constants.Constants;

import java.util.regex.Pattern;

public class Utils {

    private static final Pattern lettersPattern = Pattern.compile(Constants.LETTERS_PATTERN);
    private static final Pattern numericPattern = Pattern.compile(Constants.NUMERIC_PATTERN);
    private static final Pattern addressPattern = Pattern.compile(Constants.ADDRESS_PATTERN);
    private static final Pattern emailPattern = Pattern.compile(Constants.EMAIL_PATTERN);
    private static final Pattern passwordPattern = Pattern.compile(Constants.PASSWORD_PATTERN);
    private static final Pattern passwordSizePattern = Pattern.compile(Constants.PASSWORD_SIZE_PATTERN);

    public static void validateUser(User user) throws BadRequestException {
        if (user.getFirstName() == null || !lettersPattern.matcher(user.getFirstName()).matches()) {
            throw new BadRequestException(Constants.USER_INVALID_FIRST_NAME_MESSAGE);
        }

        if (user.getLastName() == null || !lettersPattern.matcher(user.getLastName()).matches()) {
            throw new BadRequestException(Constants.USER_INVALID_LAST_NAME_MESSAGE);
        }

        if (user.getPhoneNumber() == null || !numericPattern.matcher(user.getPhoneNumber().toString()).matches()) {
            throw new BadRequestException(Constants.USER_INVALID_PHONE_NUMBER_MESSAGE);
        }

        if (user.getAddress() == null || !addressPattern.matcher(user.getAddress()).matches()) {
            throw new BadRequestException(Constants.USER_INVALID_ADDRESS_MESSAGE);
        }

        if (user.getEmail() == null || !emailPattern.matcher(user.getEmail()).matches()) {
            throw new BadRequestException(Constants.USER_INVALID_EMAIL_MESSAGE);
        }

        if (user.getPassword() == null || !passwordPattern.matcher(user.getPassword()).matches()) {
            throw new BadRequestException(Constants.USER_INVALID_PASSWORD_MESSAGE);
        }

        if (!passwordSizePattern.matcher(user.getPassword()).matches()) {
            throw new BadRequestException(Constants.USER_INVALID_PASSWORD_SIZE_MESSAGE);
        }
    }

}
