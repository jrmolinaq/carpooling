package com.jrmolinaq.carpooling.business;

import com.jrmolinaq.carpooling.model.User;

public interface UserService {

    User findUserById(String userId) throws Exception;

    User saveUser(User user) throws Exception;
}
