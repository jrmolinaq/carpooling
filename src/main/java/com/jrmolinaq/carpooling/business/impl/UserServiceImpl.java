package com.jrmolinaq.carpooling.business.impl;

import com.jrmolinaq.carpooling.business.UserService;
import com.jrmolinaq.carpooling.exceptions.BadRequestException;
import com.jrmolinaq.carpooling.exceptions.UserAlreadyExistsException;
import com.jrmolinaq.carpooling.model.User;
import com.jrmolinaq.carpooling.repository.UserRepository;
import com.jrmolinaq.carpooling.util.Utils;
import com.jrmolinaq.carpooling.util.constants.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private final Pattern userIdPattern = Pattern.compile(Constants.NUMERIC_PATTERN);

    @Override
    public User findUserById(String userIdStr) throws BadRequestException {

        if (!userIdPattern.matcher(userIdStr).matches()) {
            throw new BadRequestException(Constants.BAD_REQUEST_USER_ID_MESSAGE);
        }

        return userRepository.findById(Integer.valueOf(userIdStr)).orElse(null);
    }

    @Override
    public User saveUser(User user) throws Exception {
        if (user != null && user.getUserId() != null && userRepository.findById(user.getUserId()).isPresent()) {
            throw new UserAlreadyExistsException(Constants.USER_ALREADY_EXISTS_MESSAGE);
        }

        Utils.validateUser(user);

        return userRepository.save(user);
    }

}
